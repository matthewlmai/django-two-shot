from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


@login_required
def receipt_list_view(request):
    receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        # aka GET
        form = ReceiptForm()
    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    expenses = ExpenseCategory.objects.filter(owner=request.user)
    context = {"expense": expenses}
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    account = Account.objects.filter(owner=request.user)
    context = {"accounts": account}
    return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        # aka GET
        form = ExpenseForm()
    context = {"form": form}

    return render(request, "receipts/create_categories.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        # aka GET
        form = AccountForm()
    context = {"form": form}

    return render(request, "receipts/create_account.html", context)
